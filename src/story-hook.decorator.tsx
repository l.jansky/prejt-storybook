import React from 'react';

class HookStory extends React.Component<any> {
	componentDidMount() {
		this.props.beforeCallback && this.props.beforeCallback();
	}

	componentWillUnmount() {
		this.props.afterCallback && this.props.afterCallback();
	}

	render() {
		return <>{this.props.children}</>;
	}
}

export const afterStory = (afterStoryCallback: () => void) => (
	storyFn: any
) => {
	return <HookStory afterCallback={afterStoryCallback}>{storyFn()}</HookStory>;
};

export const beforeStory = (beforeStoryCallback: () => void) => (
	storyFn: any
) => {
	return (
		<HookStory beforeCallback={beforeStoryCallback}>{storyFn()}</HookStory>
	);
};
