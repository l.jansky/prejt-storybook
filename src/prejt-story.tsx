import React from 'react';
import fetchMock, { MockResponse } from 'fetch-mock';
import {
	ComponentsContextProvider,
	PipeContextProvider,
	StoreContextProvider,
	Container,
	ComponentConfig,
	getStore,
	ModuleOpener,
	ComponentsDefinitions
} from '@prejt/ui-core';
import withPrejt from './index';
import { storybookAction } from './action-pipe-function';
import { afterStory, beforeStory } from './story-hook.decorator';

type PrejtFetchMockFunction = (url: string) => MockResponse;
type PrejtFetchMockResponse = MockResponse | PrejtFetchMockFunction;

const storybookPipeFunctions = {
	action: storybookAction
};

export const getPrejtStory = (
	componentsDefinitions: ComponentsDefinitions,
	WrapComponent: React.FC = React.Fragment
) => (componentConfigs: ComponentConfig[], initialData = {}) => {
	const story = (args: any, context: any) => {
		const store = context.parameters.prejt.store;
		return (
			<div style={{ backgroundColor: '#fff', padding: '20px' }}>
				<WrapComponent>
					<ComponentsContextProvider
						componentsDefinitions={componentsDefinitions}
					>
						<PipeContextProvider functions={storybookPipeFunctions}>
							<StoreContextProvider store={store}>
								<Container
									childConfigs={componentConfigs}
									dataPath={[]}
									eventContext={['root']}
								/>
								<ModuleOpener />
							</StoreContextProvider>
						</PipeContextProvider>
					</ComponentsContextProvider>
				</WrapComponent>
			</div>
		);
	};

	story.story = {
		decorators: [
			withPrejt,
			afterStory(() => {
				fetchMock.reset();
			})
		],
		parameters: {
			prejt: {
				store: getStore(initialData)
			}
		}
	};

	story.fetchMock = (matcher: string, response: PrejtFetchMockResponse) => {
		story.story.decorators.push(
			beforeStory(() => {
				fetchMock.mock(matcher, url => {
					let fetchResponse;
					if (typeof response === 'function') {
						fetchResponse = response(url);
					} else {
						fetchResponse = response;
					}

					console.log('MOCK FETCH', url, fetchResponse);
					return fetchResponse;
				});
			})
		);

		return story;
	};

	return story;
};
