import React from 'react';
import { useChannel } from '@storybook/api';

const ListItem: React.FC<any> = ({ label, data, setSelectedItem, path }) => {
	if (typeof data === 'object') {
		return (
			<>
				<li onClick={() => setSelectedItem({ data, path })}>{label}:</li>
				<ul>
					{Object.keys(data).map(key => (
						<ListItem
							label={key}
							key={key}
							data={data[key]}
							setSelectedItem={setSelectedItem}
							path={[...path, key]}
						/>
					))}
				</ul>
			</>
		);
	} else {
		return (
			<li onClick={() => setSelectedItem({ data, path })}>
				{label}: {String(data)}
			</li>
		);
	}
};

const parseStringData = (stringData: string) => {
	try {
		return JSON.parse(stringData);
	} catch (e) {
		return stringData;
	}
};

const getStringData = (data: any) => {
	if (typeof data === 'undefined') {
		return '';
	} else if (typeof data === 'object') {
		return JSON.stringify(data);
	} else {
		return data;
	}
};

export const PrejtPanel: React.FC = () => {
	const [storeData, setStoreData] = React.useState({});
	const [selectedItem, setSelectedItem] = React.useState({
		path: [],
		data: undefined
	});

	const emit = useChannel({
		'prejt/event': setStoreData
	});

	const data: any = storeData || {};

	return (
		<div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
			<div style={{ flex: '60%' }}>
				<ul>
					{Object.keys(data).map(key => (
						<ListItem
							label={key}
							key={key}
							data={data[key]}
							path={[key]}
							setSelectedItem={setSelectedItem}
						/>
					))}
				</ul>
			</div>
			<div style={{ flex: '40%' }}>
				<div>
					{selectedItem.path.join('.')}
					<textarea
						style={{ width: '100%' }}
						value={getStringData(selectedItem.data)}
						onChange={e =>
							setSelectedItem({
								...selectedItem,
								data: parseStringData(e.target.value)
							})
						}
					/>
				</div>
				<div>
					<button onClick={() => emit('prejt/set-data', selectedItem)}>
						Set data
					</button>
				</div>
			</div>
		</div>
	);
};
