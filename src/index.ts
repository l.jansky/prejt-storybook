import addons, { makeDecorator } from '@storybook/addons';
export * from './prejt-story';

export default makeDecorator({
	name: 'withPrejt',
	parameterName: 'prejt',
	// This means don't run this decorator if the notes decorator is not set
	skipIfNoParametersOrOptions: true,
	wrapper: (getStory, context, { parameters }) => {
		const channel = addons.getChannel();
		const store = parameters.store;
		store.subscribe([], (data: any) => {
			channel.emit('prejt/event', data);
		});

		channel.emit('prejt/event', store.getData());

		channel.on('prejt/set-data', data => {
			store.setValue(data.path, data.data);
		});

		return getStory(context);
	}
});
