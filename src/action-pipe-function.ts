import { PipeFunction } from '@prejt/ui-core';
import { action } from '@storybook/addon-actions';

export const storybookAction: PipeFunction = ({
	input,
	params: [actionName],
	next
}) => {
	const name =
		actionName && typeof actionName.value === 'string' ? actionName.value : '';
	const actionHandler = action(name);
	actionHandler(input);
	return next(input.value);
};
