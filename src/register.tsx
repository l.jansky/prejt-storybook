import React from 'react';
import { addons, types } from '@storybook/addons';
import { AddonPanel } from '@storybook/components';
import { PrejtPanel } from './prejt-panel';

const ADDON_ID = 'prejt';
const PARAM_KEY = 'prejt';
const PANEL_ID = `${ADDON_ID}/panel`;

addons.register(ADDON_ID, api => {
	const render: any = ({ active, key }: any) => (
		<AddonPanel active={active} key={key}>
			<PrejtPanel />
		</AddonPanel>
	);
	const title = 'Prejt';

	addons.add(PANEL_ID, {
		type: types.PANEL,
		title,
		render,
		paramKey: PARAM_KEY
	});
});
